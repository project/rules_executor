<?php


/**
 * @file
 * The Rules Executor Views display plugin.
 *
 * This is the main Rules Executor display plugin that allows our style and
 * row style plugins to hook into the right displays. We also provide a way to
 * specify the execution interval for this display.
 */

class rules_executor_plugin_display_rules_executor extends views_plugin_display {
  function get_style_type() { return 'rules_executor'; }

  function defaultable_sections($section = NULL) {
    if (in_array($section, array('style_options', 'style_plugin', 'row_options', 'row_plugin', ))) {
      return FALSE;
    }

    $sections = parent::defaultable_sections($section);

    return $sections;
  }

  function option_definition() {
    $options = parent::option_definition();

    $options['displays'] = array('default' => array());

    // Overrides for standard stuff:
    $options['style_plugin']['default'] = 'rules_executor_style';
    $options['style_options']['default']  = array('interval' => 60 * 60);
    $options['row_plugin']['default'] = 'rules_executor_row_ruleset';
    $options['defaults']['default']['style_plugin'] = FALSE;
    $options['defaults']['default']['style_options'] = FALSE;
    $options['defaults']['default']['row_plugin'] = FALSE;
    $options['defaults']['default']['row_options'] = FALSE;
    $options['interval'] = array('default' => 60*60);

    return $options;
  }

  function _option_intervals() {
    static $computed_options;

    if (!is_null($computed_options)) {
      return $computed_options;
    }

    $options = array(
      60 * 30, // 30 Minutes
      60 * 60, // 1 Hour
      60 * 60 * 6, // 6 Hours
      60 * 60 * 12, // 12 Hours
      60 * 60 * 24, // 24 Hours
      60 * 60 * 24 * 7, // 7 Days
      60 * 60 * 24 * 28, // 28 Days
      60 * 60 * 24 * 365, // 365 Days
    );

    $computed_options = drupal_map_assoc($options, 'format_interval');

    // Add an option to trigger the action every time the cron is launched, at
    // the beginning of the list and an option to never execute at the end:
    $computed_options = array(0 => t('As often as possible (All cron calls)'))
      + $computed_options
      + array(-1 => t('Never execute automatically (No cron calls)'));


    return $computed_options;
  }

  /**
   * Provide the summary for page options in the views UI.
   *
   * This output is returned as an array.
   */
  function options_summary(&$categories, &$options) {
    // It is very important to call the parent function here:
    parent::options_summary($categories, $options);

    $categories['rules_executor'] = array(
      'title' => t('Rules Executor'),
    );
    $intervals = $this->_option_intervals();
    $interval = $intervals[$this->get_option('interval')];

    $options['interval'] = array(
      'category' => 'rules_executor',
      'title' => t('Interval'),
      'value' => $interval,
    );
  }

  /**
   * Provide the default form for setting options.
   */
  function options_form(&$form, &$form_state) {
    // It is very important to call the parent function here:
    parent::options_form($form, $form_state);

    switch ($form_state['section']) {
      case 'interval':
        $form['#title'] .= t('Execution interval');
        $form['#help_topic'] = 'interval';
        $form['#help_module'] = 'rules_executor';
        $form['interval'] = array(
          '#type' => 'select',
          '#description' => t('Choose how often this view should be automatically executed.'),
          '#default_value' => $this->get_option('interval'),
          '#options' => $this->_option_intervals(),
        );
        break;
    }
  }

  function options_submit(&$form, &$form_state) {
    // It is very important to call the parent function here:
    parent::options_submit($form, $form_state);
    switch ($form_state['section']) {
      case 'interval':
        $this->set_option('interval', $form_state['values']['interval']);
        break;
    }
  }

  function get_interval() {
    return $this->get_option('interval');
  }
}
